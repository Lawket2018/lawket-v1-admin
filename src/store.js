import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    lawketBaseUrl: 'http://192.168.1.220:8080/lawket_php/',
    lawketImageUrl: 'http://192.168.1.220:8080/lawket_php/images/',
    baekyangBaseUrl: 'http://newbird15.cafe24.com/data/app_php/',

    adminFlag: false,                     //for login with Admin
    adminData: [],                        //for login with Administrator content

    check_terms: {
      lo_terms: '',       //이용약관
      lo_offer: '',       //개인정보 제3자 제공
      lo_marketing: '',   //마케팅 정보수신
    },
  },
});