import Vue from 'vue'
import Router from 'vue-router'
import AdminHome from '@/components/AdminHome'
import CreateAdmin from '@/components/CreateAdmin'
import CreateAdType from '@/components/CreateAdType'
import CreateInfomation from '@/components/CreateInfomation'
import CreateConsult from '@/components/CreateConsult'
import CreateFaq from '@/components/CreateFaq'
import CreateNotice from '@/components/CreateNotice'
import CreateUser from '@/components/CreateUser'
import CreateCustomer from '@/components/CreateCustomer'
import RequestAd from '@/components/RequestAd'
import CreateTerms from '@/components/CreateTerms'
import CreateResponse from '@/components/CreateResponse'
import AdList from '@/components/AdList'
import CreateSearchText from '@/components/CreateSearchText'
//import UploadFile from '@/components/UploadFile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AdminHome',
      component: AdminHome
    },
    {
      path: '/CreateAdmin',
      name: 'CreateAdmin',
      component: CreateAdmin
    },
    {
      path: '/CreateAdType',
      name: 'CreateAdType',
      component: CreateAdType
    },
    {
      path: '/CreateInfomation',
      name: 'CreateInfomation',
      component: CreateInfomation
    },
    {
      path: '/CreateConsult',
      name: 'CreateConsult',
      component: CreateConsult
    },
    {
      path: '/CreateFaq',
      name: 'CreateFaq',
      component: CreateFaq
    },
    {
      path: '/CreateNotice',
      name: 'CreateNotice',
      component: CreateNotice
    },
    {
      path: '/CreateUser',
      name: 'CreateUser',
      component: CreateUser
    },
    {
      path: '/CreateCustomer',
      name: 'CreateCustomer',
      component: CreateCustomer
    },
    {
      path: '/RequestAd',
      name: 'RequestAd',
      component: RequestAd
    },
    {
      path: '/CreateTerms',
      name: 'CreateTerms',
      component: CreateTerms
    },
    {
      path: '/CreateResponse',
      name: 'CreateResponse',
      component: CreateResponse
    },
    {
      path: '/AdList',
      name: 'AdList',
      component: AdList
    },
    {
      path: '/CreateSearchText',
      name: 'CreateSearchText',
      component: CreateSearchText
    }
  ]
})